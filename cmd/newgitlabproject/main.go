// main is a command for ....    
package main

import (
  "flag"
  "fmt"
  "log"
  //dsa-2000/mnc/code/newgitlabproject/internal/example"
)

var (
    // Version is the git version at build time. See Makefile
	Version string
    // Build is git hash at buid time. See Makefile
	Build   string
)

func main() {
  log.Println("Version: ", Version)
  log.Println("Build: ", Build)

  configFnPtr := flag.String("c", "newgitlabprojectConfig.yml", "Config file")
  flag.Parse()

  fmt.Printf("TODO: Reading Config from: %s\n", *configFnPtr)
 
  // example using internal package. See import above.   
  //rtn := example.Ex("Go")
  //log.Println(rtn)
}
